﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScrissorsLizardMan
{
    public enum Choice
    {
        Rock, 
        Paper, 
        Scissors, 
        Lizard, 
        Human
    }

    class Rule
    {
        #region fields

        public readonly Choice Winner;
        public readonly Choice Loser;

        #endregion

        #region constructor

        public Rule(Choice winner, Choice loser)
        {
            Winner = winner;
            Loser = loser;
        }

        #endregion 

    }

}
