﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScrissorsLizardMan
{
    class Decision
    {

        #region fields
        private Player _winningPlayer;
        private Player _losingPlayer;

        /// <summary>
        /// These are my pre-defined game rules, I choose this kind of solution, because this way, the game logic and the rules are easily expandable later. First I wanted to use
        /// different classes for each rule, but I realized if I do so, at every new rule introduced, all classes have to be modified.
        /// </summary>
        static List<Rule> Rules = new List<Rule> {

            new Rule(Choice.Scissors, Choice.Paper),
            new Rule(Choice.Paper, Choice.Rock),
            new Rule(Choice.Rock, Choice.Lizard),
            new Rule(Choice.Lizard, Choice.Human),
            new Rule(Choice.Human, Choice.Scissors),
            new Rule(Choice.Scissors, Choice.Lizard),
            new Rule(Choice.Lizard, Choice.Paper),
            new Rule(Choice.Paper, Choice.Human),
            new Rule(Choice.Human, Choice.Rock),
            new Rule(Choice.Rock, Choice.Scissors)

        };

        #endregion

        #region constructors

        private Decision(Player winningPlayer, Player losingPlayer)
        {
            _winningPlayer = winningPlayer;
            _losingPlayer = losingPlayer;
        }

        #endregion

        #region properties
        public Player WinningPlayer
        {
            get
            {
                return _winningPlayer;
            }
        }

        public Player LosingPlayer
        {
            get
            {
                return _losingPlayer;
            }
        }

        #endregion


        /// <summary>
        /// This method decides who is the winner and the loser, out of 2 players, and saves who played with who..
        /// </summary>
        /// <param name="player1"></param>
        /// <param name="player2"></param>
        /// <returns></returns>

        #region methods
        public static Decision Decide(Player player1, Player player2)
        {
            var rule = FindWinningRule(player1.PlayerChoice, player2.PlayerChoice);
            
            if (rule != null)
            {
                player1.PlayedWith.Add(player2);
                player2.PlayedWith.Add(player1);

                return new Decision(player1, player2);   
            }

            rule = FindWinningRule(player2.PlayerChoice, player1.PlayerChoice);
            if (rule != null)
            {
                player2.PlayedWith.Add(player1);
                player1.PlayedWith.Add(player2);
                

                return new Decision(player2, player1);   
            }

            List<Player> shuffledList = new List<Player>();                         
            shuffledList = Player.MakeRandomPlayerList(player1, player2);

            player1.PlayedWith.Add(shuffledList[1]);
            player2.PlayedWith.Add(shuffledList[0]);

            return new Decision(shuffledList[0], shuffledList[1]);
        }

        /// <summary>
        /// This method finds the rule which will be used, to decide the winner.
        /// </summary>
        /// <param name="player1Choice"></param>
        /// <param name="player2Choice"></param>
        /// <returns></returns>
        private static Rule FindWinningRule(Choice player1Choice, Choice player2Choice)
        {
            return Rules.FirstOrDefault(r => r.Winner == player1Choice && r.Loser == player2Choice);
        }

        #endregion

    }
}
