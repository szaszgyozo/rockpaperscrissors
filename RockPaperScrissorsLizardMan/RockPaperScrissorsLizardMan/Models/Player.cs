﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScrissorsLizardMan
{
    public class Player
    {

        #region fields

        private List<Player> _playedWith;

        #endregion

        #region constructor
        public Player()
        {
            _playedWith = new List<Player>();
        }

        #endregion


        #region properties
        public string Name { get; set; }
        public Choice PlayerChoice { get; set; }
        public List<Player> PlayedWith
        {
            get
            {
                return _playedWith;
            }
            set
            {
                _playedWith = value;
            }
        }

        #endregion


        #region methods
        /// <summary>
        /// This method creates all the player based on an int number.
        /// </summary>
        /// <param name="noOfPlayers"></param>
        public static void CreateANumberOfPlayers(int noOfPlayers)
        {
            
            for (int i = 1; i <= noOfPlayers; i++)
            {
                if (noOfPlayers <= 8)
                {
                    GameManager.Instance.Players.Add(new Player { Name = i.ToString() });
                }
                else
                {
                    GameManager.Instance.Players.Add(new Player { Name = i.ToString(), PlayerChoice = Player.GetRandomChoice() });
                }
            }
        }

        /// <summary>
        /// This method creates a shuffled player list out of 2 players.
        /// </summary>
        /// <param name="player1"></param>
        /// <param name="player2"></param>
        /// <returns></returns>
        public static List<Player> MakeRandomPlayerList(Player player1, Player player2)
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());

            List<Player> playerList = new List<Player>();
            playerList.Add(player1);
            playerList.Add(player2);

            var shuffledList = playerList.OrderBy(item => random.Next());

            return shuffledList.ToList();

        }

        /// <summary>
        /// This method returns a Choice randomly.
        /// </summary>
        /// <returns></returns>
        public static Choice GetRandomChoice()
        {
            Array values = Enum.GetValues(typeof(Choice));
            Random random = new Random(Guid.NewGuid().GetHashCode());
            Choice randomChoice = (Choice)values.GetValue(random.Next(0, values.Length));

            return randomChoice;
        }

        public override string ToString()
        {
            return (Name + "-" + PlayerChoice.ToString("G")).ToString();
        }

        /// <summary>
        /// This method returns a string, with playernames which a specified player played with.
        /// </summary>
        /// <returns></returns>
        public string GetPlayedWithString()
        {
            string playedWIthString = "Played with: ";

            foreach(Player player in this.PlayedWith)
            {
                playedWIthString += player.Name + " ";
            }

            return playedWIthString;

        }

        #endregion
    }
}
