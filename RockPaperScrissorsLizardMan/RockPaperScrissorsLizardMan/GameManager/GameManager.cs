﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScrissorsLizardMan
{

    public sealed class GameManager
    {
        #region Fields

        private static GameManager _instance;

        private static object _locker = new object();

        private int _numberOfPlayers;

        private List<Player> _players = new List<Player>();


        #endregion

        #region Constructors

        private GameManager()
        {

        }

        #endregion

        #region Properties

        public static GameManager Instance
        {
            get
            {
                lock (_locker)
                {
                    if (_instance == null)
                    {
                        _instance = new GameManager();
                    }
                    return _instance;
                }
            }
        }


        public int NumberOfPlayers
        {
            get
            {
                return _numberOfPlayers;
            }
            set
            {
                _numberOfPlayers = value;
            }
        }

        public List<Player> Players
        {
            get
            {
                return _players;
            }
            set
            {
                _players = value;
            }
        }

        #endregion

        /// <summary>
        /// This method generates every new round of the game.
        /// </summary>

        #region methods
        public void GenerateRounds()
        {
            var roundsNumber = (int)Math.Log(_numberOfPlayers, 2) +1;

            List <Player> currentPlayers = new List<Player>();
            currentPlayers = _players;

            for (int i = 1; i<= roundsNumber; i++)
            {
                
                if (i == roundsNumber)
                {
                    Console.WriteLine("");
                    Console.WriteLine("-----------------------");
                    Console.WriteLine("The Winner is..... ");
                    Console.WriteLine("-----------------------");
                }
                else
                {
                    Console.WriteLine("");
                    Console.WriteLine("-----------------------");
                    Console.WriteLine("Round {0}. ", i);
                    Console.WriteLine("-----------------------");
                }
                
                currentPlayers = PlayMatchupsAndDefineWinners(currentPlayers);
            }

        }

        /// <summary>
        /// This method decides who is the winner of every player matchup, and writes it in the console.
        /// </summary>
        /// <param name="remainingPlayers"></param>
        /// <returns></returns>

        public List<Player> PlayMatchupsAndDefineWinners(List<Player> remainingPlayers)
        {
            List<Player> winners = new List<Player>();

            for(int i = 0; i < remainingPlayers.Count; i+=2)
            {
                if (remainingPlayers.Count == 1)
                {
                    Console.WriteLine("{0}", remainingPlayers[i]);
                    Console.WriteLine("{0}", remainingPlayers[i].GetPlayedWithString());
                    break;
                }


                Console.WriteLine("{0} vs. {1}", remainingPlayers[i].ToString(), remainingPlayers[i + 1].ToString());

                var decision = Decision.Decide(remainingPlayers[i], remainingPlayers[i + 1]);

                winners.Add(decision.WinningPlayer);
            }

            return winners;

        }

        #endregion

    }
}