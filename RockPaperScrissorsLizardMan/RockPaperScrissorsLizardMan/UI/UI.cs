﻿using ConsoleTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScrissorsLizardMan
{
    public class UI
    {
        #region fields

        private bool _exit;

        #endregion

        #region constructors

        public UI()
        {

        }

        #endregion

        #region methods

        /// <summary>
        /// This method runs the main UI window, by showing the main menu initially and waiting for user input.
        /// </summary>
        public void RunUI()
        {
            InitializeMainMenu();

            WaitForInputCommandAndCatchExceptions();

        }

        /// <summary>
        /// This method prints out the game title, and calls for the command list of the main menu.
        /// </summary>
        private void InitializeMainMenu()
        {
            Console.WriteLine("Welcome to my Rock-Paper-Scrissors-Lizard-Human Game Simulation!");
            Console.WriteLine("----------------------------------------------------------------");
            Console.WriteLine("");
            Console.WriteLine("");

            ShowCommandList();
        }


        /// <summary>
        /// This method prints out the command list of the main menu.
        /// </summary>
        private void ShowCommandList()
        {

            var table = new ConsoleTable("Commands", "");

            Console.WriteLine("");
            table.AddRow("cmd", "Show command list");
            table.AddRow("players", "Define nr. of players your want to simulate with");
            //table.AddRow("sign ", "Define sign for every player you added (above 8 players, the program automatically assigns it.)");
            table.AddRow("simulate ", "Run simulation");
            table.AddRow("exit", "Exit shop");
            table.Write(Format.Minimal);

        }

        /// <summary>
        /// This method waits for user input in the main menu and catches unwanted exceptions.
        /// </summary>
        private void WaitForInputCommandAndCatchExceptions()
        {
            string inputString = "";

            while (!_exit)
            {
                Console.WriteLine("");
                try
                {
                    inputString = Console.ReadLine();
                    ReadCommandInput(inputString);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        /// <summary>
        /// This method reads the user`s input in the main menu, and decides what to do with it.
        /// </summary>
        /// <param name="inputStr"></param>
        private void ReadCommandInput(string inputStr)
        {
            bool validCommand = false;

            switch(inputStr) 
            {
                case "cmd":

                    ShowCommandList();
                    validCommand = true;
                    Console.WriteLine("");

                    break;

                case "players":

                    AskForNumberOfPlayers();
                    validCommand = true;
                    Console.WriteLine("");

                    break;

                case "simulate":

                    Simulate();
                    validCommand = true;

                    break;

                case "exit":

                    _exit = true;
                    Console.WriteLine("Byebye.......:)");
                    validCommand = true;

                    break;

            }


            if (!validCommand)
            {
                throw new Exception("Wrong input! Try again.");
            }

        }

        /// <summary>
        /// This method asks the user for number of players and invokes the player creating method in the GameManager.
        /// </summary>
        public void AskForNumberOfPlayers()
        {
            Console.WriteLine("Number of players: ");

            string noOfPlayersString = Console.ReadLine();

            if (IsPowerOfTwo(Int32.Parse(noOfPlayersString)))
            {
                GameManager.Instance.NumberOfPlayers = Int32.Parse(noOfPlayersString);
                Player.CreateANumberOfPlayers(GameManager.Instance.NumberOfPlayers);

                Console.WriteLine("{0} players generated.", noOfPlayersString);
                Console.WriteLine("");
                Console.WriteLine("Write 'simulate' to run your simulation.");


                if (GameManager.Instance.NumberOfPlayers <= 8)
                {
                    AskForPlayerChoices();
                }

            }
            else
            {
                Console.WriteLine("The number is not a power of 2.");
                Console.WriteLine("");
                AskForNumberOfPlayers();

            }

        }

        /// <summary>
        /// This method runs the game simulation, invoking the proper method in the GameManager.
        /// </summary>
        public void Simulate()
        {
            GameManager.Instance.GenerateRounds();
        }


        /// <summary>
        /// This method asks the user to define player choice manually if the game is runing with 2, 4 or 8 players.
        /// </summary>
        public void AskForPlayerChoices()
        {
            Console.WriteLine("Please choose a sign for all the {0} players participating:", GameManager.Instance.NumberOfPlayers.ToString());
            Console.WriteLine("R - Rock, P - Paper, S - Scrissors, L - Lizard, H - Human");
            Console.WriteLine("");


            foreach(var player in GameManager.Instance.Players)
            {
                SetChoice(player);
            }

            Console.WriteLine("Write 'simulate' to run your simulation.");

        }
        /// <summary>
        /// This method sets the choosen player choice.
        /// </summary>
        /// <param name="player"></param>
        public void SetChoice(Player player)
        {
            Console.WriteLine("Choose a sign for Player {0}: ", player.Name);

            string inputStr = Console.ReadLine();

            switch (inputStr.ToUpper())
            {
                case "R":
                    player.PlayerChoice = Choice.Rock;
                    break;

                case "P":
                    player.PlayerChoice = Choice.Paper;
                    break;

                case "S":
                    player.PlayerChoice = Choice.Scissors;
                    break;

                case "L":
                    player.PlayerChoice = Choice.Lizard;
                    break;

                case "H":
                    player.PlayerChoice = Choice.Human;
                    break;
                default:
                    Console.WriteLine("Please select a correct sign! ( R - Rock, P - Paper, S - Scrissors, L - Lizard, H - Human )");
                    SetChoice(player);
                    break;
            }
        }

        /// <summary>
        /// This method decides if the inputed player number is the power of 2 or not.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        bool IsPowerOfTwo(int x)
        {
            return (x != 0) && ((x & (x - 1)) == 0);
        }

        #endregion

    }
}
